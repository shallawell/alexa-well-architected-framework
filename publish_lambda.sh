LAMBDA_NAME=alexa-wellArchitected
BUCKET=alexa-well-arch
ZIPFILE=lambda.zip
REGION=us-east-1
rm $ZIPFILE
cd lambda
npm install
zip -r ../$ZIPFILE * -x "*mp3*"
cd ..
echo "starting s3://$BUCKET/$ZIPFILE.. this can take a couple of mins"
aws s3 cp lambda.zip s3://$BUCKET
echo "upload to s3://$BUCKET/$ZIPFILE"
echo "upload done"
echo "starting $LAMBDA_NAME lambda function update, upload from s3://$BUCKET/$ZIPFILE"
aws lambda update-function-code --function-name $LAMBDA_NAME --s3-bucket $BUCKET --s3-key $ZIPFILE --region $REGION
echo "publish_lambda.sh completed ---------"

var Alexa = require('alexa-sdk');

// Helpers
var convertArrayToReadableString = require('./helpers/convertArrayToReadableString');

exports.handler = function(event, context, callback){
  var alexa = Alexa.handler(event, context);
  alexa.registerHandlers(handlers);
  alexa.execute();
};

var handlers = {

  'LaunchRequest': function () {
    this.emit(':ask', `Welcome to well architected framework. 
     You can say, what are the 5 pillars, or, ask for an overview`, 
     'try saying hello');
  },

  'Hello': function () {
    this.emit(':tell', 'Hi there! Ask for an overview or about the well architected pillars');
  },

  'waOverview': function () {
    this.emit(':ask', `The Well-Architected framework has been developed to help cloud architects build the most secure, high-performing, resilient, and efficient infrastructure possible for their applications. This framework provides a consistent approach for customers and partners to evaluate architectures, and provides guidance to help implement designs that will scale with your application needs over time. 
     Ask about the 5 pillars. Or ask about the 5 pillars or about the well architected review`, 
     'Ask about the 5 pillars?');
  },

  'waPillars': function () {
    this.emit(':ask', `The AWS Well-Architected Framework has 5 key pillars. Operational Excellence, Security, Reliability, Performance Efficiency and, Cost Optimization. 
     Ask about a specific pillar for more information. Or ask for an example question, such as what is a cost question`, 
     'Ask about a particular pillar?');
  },

  'waSecurity': function () {
    this.emit(':ask', `The security pillar focuses on protecting information and systems. Key topics include confidentiality and integrity of data, identifying and managing who can do what with privilege management, protecting systems, and establishing controls to detect security events. 
     Ask me about another pillar, like operational excellence or cost optimisation, or about the security design principles`, 
     'Ask about another pillar?');
  },

  'waSecQ': function () {
    this.emit(':ask', `Security question six. How are you leveraging a.w.s. service level security features. Consider services such as s3 bucket policies or k.m.s. key policies. 
     Ask for another pillar question, or security design principles`, 
     'Ask for another pillar question?');
  },

  'waSecurityDesign': function () {
    this.emit(':ask', `The security design priciples are. Apply security at all layers. Enable traceability. Implement a principle of least privilege. Focus on securing your system. Automate security best practices. 
     Ask me about another pillar, or for an example security question`, 
     'Ask me about another design principle?');
  },

  'waOpEx': function () {
    this.emit(':ask', `The operational excellence pillar focuses on running and monitoring systems to deliver business value, and continually improving processes and procedures. Key topics include managing and automating changes, responding to events, and defining standards to successfully manage daily operations. 
     Ask me about another pillar, like relibility or cost, or operations design principles`, 
     'Ask about another pillar?');
  },

  'waOpExQ': function () {
    this.emit(':ask', `Operational Excellence question four. How do you monitor your workload to ensure it is operating as expected. Consider services such as logging and cloud watch to monitor for performance degradation over time. 
     Ask for another pillar question, like reliability or cost, or about operational excellence design principles`, 
     'Ask for another pillar question?');
  },

  'waOpExDesign': function () {
    this.emit(':ask', `The operational excellent design priciples are. Perform Operations with Code. Align Operations Processes to Business Objectives. Make Regular, Small, Incremental Changes. Test for Responses to Unexpected Events. Learn from Operational Events and Failures. Keep Operations Procedures Current. 
     Ask about another pillar, like reliability or cost, or for an example operations question`, 
     'Ask me about another design principle?');
  },

  'waReliability': function () {
    this.emit(':ask', `The reliability pillar focuses on the ability to prevent, and quickly recover from failures to meet business and customer demand. Key topics include foundational elements around setup, cross project requirements, recovery planning, and how we handle change. 
     Ask me about another pillar suck as security or performance, or about reliability design principles`, 
     'Ask about another pillar?');
  },

  'waReliabilityQ': function () {
    this.emit(':ask', `Reliability question one. How do you managing a.w.s. service limits for your accounts. Accounts have defaults limits for resouces and these needs to be managed to allow the right level of scaling while still controlling the number of resources which are allowed to be used. 
     Ask for another pillar question, like cost or security, or about reliability design principles`, 
     'Ask for another pillar question?');
  },

  'waReliabilityDesign': function () {
    this.emit(':ask', `The reliability design priciples are. Test recovery procedures. Automatically recover from failure. Scale horizontally to increase aggregate system availability. Stop guessing capacity. Manage change in automation. 
     Ask me about another pillar, like security or cost, or about the reliability design principles`, 
     'Ask me about another design principle?');
  },

  'waPerformance': function () {
    this.emit(':ask', `The performance efficiency pillar focuses on using I.T. and computing resources efficiently. Key topics include selecting the right resource types and sizes based on workload requirements, monitoring performance, and making informed decisions to maintain efficiency as business needs evolve. 
     Ask me about another pillar such as cost or reliability, or about Performance Efficiency design principles`, 
     'Ask about another pillar?');
  },

  'waPerformanceQ': function () {
    this.emit(':ask', `Performance Efficiency question eight. How do you use trade offs to improve performance. When designing solutions, actively think about the trade off to create the optimal approach. Often you can trade consistency, durability, space vs time, latency, etc to deliver higher performance by sacrificing some other constraint. 
     Ask for another pillar question, or about performance efficiency design principles`, 
     'Ask for another pillar question?');
  },

  'waPerformanceDesign': function () {
    this.emit(':ask', `The performance efficiency design priciples are. Democratize advanced technologies. Go global in minutes. Use serverless architectures. Experiment more often. Mechanical sympathy. 
     Ask me about another pillar, like security or performance, or for an example performance question`, 
     'Ask me about another design principle?');
  },

  'waCost': function () {
    this.emit(':ask', `Cost Optimization focuses on avoiding un-needed costs. Key topics include understanding and controlling where money is being spent, selecting the most appropriate and right number of resource types, analyzing spend over time, and scaling to meet business needs without overspending. 
     Ask me about another pillar, or for the cost optimisation design principles`, 
     'Ask about another pillar?');
  },

  'waCostQ': function () {
    this.emit(':ask', `Cost optimisation question six. How are you monitoring usage and spending. Establish procedures to monitor usage and spend, and leverage a.w.s. tools such as Cost Explorer, Trusted Advisor and billing alerts. 
     Ask for another pillar question, or about the cost design principles`, 
     'Ask for another pillar question?');
  },

  'waCostDesign': function () {
    this.emit(':ask', `The cost optimisation design priciples are. Adopt a consumption model. Benefit from economies of scale. Stop spending money on data center operations. Analyze and attribute expenditure. Use managed services to reduce cost of ownership. 
     Ask me about another pillar, or for an example cost question`, 
     'Ask me about another design principle?');
  },

  'waReview': function () {
    this.emit(':ask', `AWS Partners use the AWS Well-Architected Framework design principles and tools to deliver a comprehensive review of a single workload. A workload is defined as a set of machines, instances, or servers that together enable the delivery of a service or application to internal or external customers. Examples include an eCommerce website, business application, web application, ecetera. Through this review and guidance from the AWS Well-Architected Framework, can provide the customer an analysis of the workload through the lens of the WAF pillars, as well as a plan to address areas that do not comply with framework recommendations. 
     Ask who can help with a review`, 
     'How can Bulletproof help?');
  },

// TODO 
// function waDesignPrinciples
//if, security elseif Cost etc :ask 'quote pillars' 

  'waPartner': function () {
    this.emit(':ask', `a.w.s. partners such as Bulletproof Networks are amongst the few selected partners in Australia and New Zealand who is accredited to perform a well architected review. 
     Ask what is a review, who is bulletproof, or, what are the 5 pillars`, 
     'How can Bulletproof help?');
  },

  'waBulletproof': function () {
    this.emit(':ask', `Bulletproof is a premium a.w.s. partner who is accredited to perform well architected reviews. Visit www.bulletproof.net for more information.
     Ask what is a review, or, what are the 5 pillars`, 
     'How can Bulletproof help?');
  },

  'AMAZON.StopIntent': function () {
    // State Automatically Saved with :tell
    this.emit(':tell', 'Goodbye!');
  },

  'AMAZON.CancelIntent': function () {
    // State Automatically Saved with :tell
    this.emit(':tell', 'Goodbye!');
  },

  'AMAZON.HelpIntent': function () {
    this.emit(':ask', 'You can ask me about the well architected framework, the 5 pillars or a well architected review', 'What would you like to do?');
  },

  'Unhandled': function () {
    this.emitWithState('AMAZON.HelpIntent');
  }
};

module.exports = function getLondonAudio(city) {

  var londonAudio = ``;
  if (city.toLowerCase() === 'london') {
    londonAudio = `<audio src="https://s3.amazonaws.com/alexa-assets-09878/crowd-excited-alexa.mp3"/>`;
  }

  return londonAudio;
};

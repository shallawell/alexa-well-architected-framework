# alexa-well-architected-skill #

### Author @shallawell / sam.hallawell@bulletproof.net
I am developing this to further my Alexa and Lambda skills.

## Synopsis
This is the code for the Well Architected Framework Alexa skill. <br>
It uses simple intents to answer questions <br>
Alexa Invocation is "well architected framework" <br>

## Public or Private
It is currently in development and only available privately, with the intention to publish to the Alexa App store
